import com.google.common.html.HtmlEscapers;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.*;

/**
 *
 * @author Juan Olaya O
 */
public class SimplePdfToHtml {

    public static void main(String args[]) {
        System.out.println("Convirtiendo a HTML");
        if(args.length == 2){
            String docsPath = args[0];
            String docTextPath = args[1];
            lectorDocs(docsPath, docTextPath);
        } else{
            System.out.println("Requiere dos valores para poder ejecutarse");
        }
    }

    public static void lectorDocs(String docsPath, String docTextPath) {
        //Directorio donde se encuentran los documentos a ser indexados.
        File docDir = new File(docsPath);

        try {

            //Verifica si el directorio de docuimentos a convertir se puede leer y
            //si es directorio.
            if (docDir.canRead() && docDir.isDirectory()) {

                //Si listan todos los documentos del directorio.
                File[] docFiles = docDir.listFiles();

                //Revisa que la lista de documentos no sea nula.
                if (docFiles != null) {

                    //Genera un id para el documento y recorre cada archivo.
                    int docID = 0;
                    for (int i = 0; i < docFiles.length; i++) {

                        //Si uno de los elementos del directorio es un Directorio
                        //se procede a realizar el procedimeinto addDocuments
                        //a este.
                        if (docFiles[i].isDirectory()) {
                            String docDirNew = docFiles[i].getCanonicalPath();
                            lectorDocs(docDirNew, docTextPath);
                        } else {
                            //Sino es directorio se procede a leer la informacion
                            //contenida dentro del archivo. Se utiliza el lector
                            //de archivos apache tika.
                            String texto = TikaDocReader(docFiles[i]);
                            if (texto != null) {
                                passToTXT(docFiles[i], texto, docTextPath);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Error de tipo " + e.getClass() + "\n dice: " + e.getMessage());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }

    }

    //TikaDocReader se utiliza para poder indexar diferentes tipos de documentos
    //(pdf, doc, ppt, etc) y pasarlos a texto plano
    private static String TikaDocReader(File docFile) throws IOException, SAXException, TikaException {

            Parser parser = new AutoDetectParser();
            InputStream stream = new FileInputStream(docFile);
            Metadata metadata = new Metadata();
            ContentHandler handler = new BodyContentHandler(-1);
            ParseContext context = new ParseContext();
            context.set(Parser.class, parser);
            try
            {
                parser.parse(stream, handler, metadata, context);
            } catch (IOException ioException) {
                System.out.println("Error de tipo " + ioException.getClass() + "\n dice: " + ioException.getMessage());
            } finally {
                stream.close();
            }
            return handler.toString();
    }

    public static void passToTXT(File doc, String contenido, String destino) throws IOException {

        String nombre = doc.getName();
        String tipoDoc4 = nombre.substring(nombre.length() - 4);

        String htmlStart = "<html>\n" +
                "<head><title></title></head>\n" +
                "\n" +
                "<body>\n" +
                "\n" +
                "<font size=\"2\" face=\"Verdana, Geneva, sans-serif\">\n";
        String htmlEnd = "\n</font>\n" +
                "</body>\n" +
                "</html>";

        if (tipoDoc4.contains(".")) {
            String name = nombre.split(tipoDoc4)[0];
            System.out.println("Nuevo doc: " + name);

            File file = new File(destino, name + ".html");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(htmlStart + parseAccents(contenido) + htmlEnd);
            bw.flush();
            bw.close();
        }else{
            System.out.println("Revise el documento: " + nombre);
        }

    }

    public static String parseAccents(String contenido){
        String parsedContent;
        parsedContent = StringEscapeUtils.escapeHtml4(contenido);

        return parsedContent;
    }

}
